Notebook[{Cell[
BoxData[RowBox[{"Clear","[",RowBox[{"c",",","h",",","kb",",","G",",","mp"}],"]"}]],
"Input",CellLabel -> "In[37]:= ",ExpressionUUID -> "bead667c-81cb-4630-bbc9-2fd4579ae17f"],Cell[
TextData[
{StyleBox["Boltzmanns",FontWeight -> Bold],StyleBox[" ",FontWeight -> Bold],StyleBox[
"Constant",FontWeight -> Bold],StyleBox[RowBox[{StyleBox[":",FontWeight -> Bold],StyleBox[
" ",FontWeight -> Bold],StyleBox["\[Sigma]",FontWeight -> Bold],StyleBox[" ",FontWeight -> Bold],StyleBox[
"=",FontWeight -> Bold],StyleBox[" ",FontWeight -> Bold]}]],StyleBox["erg",FontWeight -> Bold],StyleBox[
"/",FontWeight -> Bold],StyleBox["cm",FontWeight -> Bold],StyleBox["^",FontWeight -> Bold],StyleBox[
"2",FontWeight -> Bold],StyleBox["/",FontWeight -> Bold],StyleBox["s",FontWeight -> Bold],StyleBox[
"/",FontWeight -> Bold],StyleBox["K",FontWeight -> Bold],StyleBox["^",FontWeight -> Bold],StyleBox[
"4",FontWeight -> Bold]}],"Text",ExpressionUUID -> "6d09589b-719a-4f84-a6ee-fde4eaa2d9bd"],Cell[
BoxData[
RowBox[{RowBox[{"boltzcons"," ","="," ",RowBox[{"5.57","*",RowBox[{"10","^",RowBox[
{"(",RowBox[{"-","5"}],")"}]}]}]}]," ",";"}]],"Input",CellLabel -> "In[38]:= ",ExpressionUUID -> "b6edb346-e7ee-4b6d-a43d-5db3d054c71d"],Cell[
CellGroupData[
{Cell[
BoxData[RowBox[{"bohrradius"," ","="," ",RowBox[{"5.29","*",RowBox[{"10","^",RowBox[
{"(",RowBox[{"-","9"}],")"}]}]}]}]],"Input",CellLabel -> "In[46]:= ",ExpressionUUID -> "feb15c2c-305d-4106-8a7b-672719fe2c4b"],Cell[
BoxData[
"5.2900000000000006`*^-9",StandardForm],"Output",CellLabel -> "Out[46]= ",ExpressionUUID -> "79b47dd3-8814-4d47-bed2-25a184c6399a"]},
Open],ExpressionUUID -> "2a365916-2010-49d7-b7c0-da3aa8a2538f"],Cell[
TextData[{"kb",StyleBox[
RowBox[{" ","="," ","["}]],"erg","]/[","K","]"}],"Text",ExpressionUUID -> "b5bcaf7b-f4f6-4e17-9337-1776eb27b29b"],Cell[
BoxData[
RowBox[{RowBox[{"kb"," ","="," ",RowBox[{"1.38","*",RowBox[{"10","^",RowBox[{"(",RowBox[
{"-","16"}],")"}]}]}]}],";"}]],"Input",CellLabel -> "In[47]:= ",ExpressionUUID -> "185f99a9-5f2a-45b7-a3b8-5ae24d6e6ab7"],Cell[
TextData[
{"G",StyleBox[RowBox[{" ","="," ","["}]],"cm","^","3","]/[","g","]/[","s","^","2","]"}],
"Text",ExpressionUUID -> "c779195b-649b-4515-9d7d-8913407a61fb"],Cell[
TextData[{"proton"," ","mass",StyleBox[
RowBox[{" ",":"}]]}],"Text",ExpressionUUID -> "28ae4c44-4a08-4447-b533-544e054b3498"],Cell[
BoxData[
RowBox[{RowBox[{"mp"," ","="," ",RowBox[{"1.672621637","*",RowBox[{"10","^",RowBox[
{"(",RowBox[{"-","24"}],")"}]}]}]}],";"}]],"Input",CellLabel -> "In[49]:= ",ExpressionUUID -> "c4748879-6955-4e44-9669-1a2ed82f3043"],Cell[
CellGroupData[
{Cell[
BoxData[RowBox[{"echarge"," ","="," ",RowBox[{"4.8032042710","*",RowBox[{"10","^",RowBox[
{"(",RowBox[{"-","10"}],")"}]}]}]}]],"Input",CellLabel -> "In[52]:= ",ExpressionUUID -> "181e7b91-db59-4085-b58a-14df5b749875"],Cell[
BoxData[
"4.803204271`*^-10",StandardForm],"Output",CellLabel -> "Out[52]= ",ExpressionUUID -> "5aabfa42-83cb-4a33-b4ff-9c8705c324dc"]},
Open],ExpressionUUID -> "62b764f4-2b10-41b4-a768-b7e3cfbb5b96"],Cell[
CellGroupData[
{Cell[
BoxData[RowBox[{"me"," ","="," ",RowBox[{"9.10938214","*",RowBox[{"10","^",RowBox[
{"(",RowBox[{"-","28"}],")"}]}]}]}]],"Input",CellLabel -> "In[53]:= ",ExpressionUUID -> "d188e93d-e553-41ee-98a0-8c08370ec157"],Cell[
BoxData[
"9.10938214`*^-28",StandardForm],"Output",CellLabel -> "Out[53]= ",ExpressionUUID -> "8941e1d4-a7d7-4753-b060-ca0d73c3dff5"]},
Open],ExpressionUUID -> "acc1c837-8523-41ee-8c80-400de214cae9"],Cell[
CellGroupData[
{Cell[
TextData[{"1",")"}],"Subsection",ExpressionUUID -> "0ec19249-3e18-45e9-8c7f-0481fa2ad184"],Cell[
BoxData[
{RowBox[{RowBox[{"\[Sigma]squared","[","v_","]"}]," ",":="," ",RowBox[{"1","/",RowBox[
{"v","^","2"}]}]}],"\n",RowBox[{RowBox[{"\[Sigma]one","[","v_","]"}]," ",":="," ",RowBox[
{"1","/","v"}]}]}],"Input",CellLabel -> "In[61]:= ",ExpressionUUID -> "f81efda2-4b77-4ea0-8fb9-44906516ddc2"],Cell[
TextData[
{"The",StyleBox[" ",FontWeight -> Bold],StyleBox["interaction",FontWeight -> Bold],StyleBox[
" ",FontWeight -> Bold],StyleBox["rate",FontWeight -> Bold],StyleBox[RowBox[{" ","<\[Sigma]"}]],"v",StyleBox[
RowBox[{">,"," "}]],"can"," ","be"," ","determined"," ","from"," ","integrating"," ","cross"," ","sections"," ","over"," ","the"," ","maxwell-boltzmann"," ","distribution",":"}],
"Text",ExpressionUUID -> "4515bf07-517a-4c26-a28b-0b96c157736c"],Cell[
BoxData[RowBox[
{RowBox[{"maxboltzdist","[",RowBox[{"m_",",","T_",",","v_"}],"]"}]," ",":="," ",RowBox[
{"4","*","Pi"," ","*",RowBox[{"v","^","2"}],"*",RowBox[{RowBox[{"(",RowBox[{"m","/",RowBox[
{"(",RowBox[{"2","*","Pi","*","kb","*","T"}],")"}]}],")"}],"^",RowBox[{"(",RowBox[
{"3","/","2"}],")"}]}],"*",RowBox[{"Exp","[",RowBox[{RowBox[{"-",RowBox[{"(",RowBox[
{"m","*",RowBox[{"v","^","2"}]}],")"}]}],"/",RowBox[{"(",RowBox[{"2","*","kb","*","T"}],")"}]}],"]"}]}]}]],
"Input",CellLabel -> "In[63]:= ",ExpressionUUID -> "25a10c92-178d-478d-b043-02d8d4055a00"],Cell[
CellGroupData[
{Cell[
BoxData[RowBox[{"Integrate","["," ",RowBox[{RowBox[{"4","*","Pi"," ","*","v","*",RowBox[
{RowBox[{"(",RowBox[{"m","/",RowBox[{"(",RowBox[{"2","*","Pi","*","k","*","T"}],")"}]}],")"}],"^",RowBox[
{"(",RowBox[{"3","/","2"}],")"}]}],"*",RowBox[{"Exp","[",RowBox[{RowBox[{"-",RowBox[
{"(",RowBox[{"m","*",RowBox[{"v","^","2"}]}],")"}]}],"/",RowBox[{"(",RowBox[{"2","*","k","*","T"}],")"}]}],"]"}]}],",",RowBox[
{"{",RowBox[{"v",",","0",",","Infinity"}],"}"}]}]," ","]"}]],"Input",CellLabel -> "In[64]:= ",
ExpressionUUID -> "37f41824-003b-404e-8fe2-318398a03faf"],Cell[
BoxData[TemplateBox[
{FractionBox[RowBox[{"k"," ",SqrtBox[FractionBox["2","\[Pi]"]]," ",SuperscriptBox[
RowBox[{"(",FractionBox["m",RowBox[{"k"," ","T"}]],")"}],RowBox[{"3","/","2"}]]," ","T"}],
"m"],RowBox[{RowBox[{"Re","[",FractionBox["m",RowBox[{"k"," ","T"}]],"]"}],">","0"}]},
"ConditionalExpression"],StandardForm],"Output",CellLabel -> "Out[64]= ",ExpressionUUID -> "dda6d2b1-3bb3-48d0-b6cd-34771e0e424a"]},
Open],ExpressionUUID -> "22750f97-64f3-4588-91ee-44a016b857f6"],Cell[
CellGroupData[
{Cell[
BoxData[RowBox[{"Integrate","["," ",RowBox[{RowBox[{"4","*","Pi"," ","*",RowBox[
{"v","^","2"}],"*",RowBox[{RowBox[{"(",RowBox[{"m","/",RowBox[{"(",RowBox[{"2","*","Pi","*","k","*","T"}],")"}]}],")"}],"^",RowBox[
{"(",RowBox[{"3","/","2"}],")"}]}],"*",RowBox[{"Exp","[",RowBox[{RowBox[{"-",RowBox[
{"(",RowBox[{"m","*",RowBox[{"v","^","2"}]}],")"}]}],"/",RowBox[{"(",RowBox[{"2","*","k","*","T"}],")"}]}],"]"}]}],",",RowBox[
{"{",RowBox[{"v",",","0",",","Infinity"}],"}"}]}]," ","]"}]],"Input",CellLabel -> "In[65]:= ",
ExpressionUUID -> "25155ddd-51ab-4758-8a21-ee9da046ef4f"],Cell[
BoxData[TemplateBox[
{"1",RowBox[{RowBox[{"Re","[",FractionBox["m",RowBox[{"k"," ","T"}]],"]"}],">","0"}]},
"ConditionalExpression"],StandardForm],"Output",CellLabel -> "Out[65]= ",ExpressionUUID -> "e9edee61-ab5a-4c4c-aeb7-4d4c60dbac3f"]},
Open],ExpressionUUID -> "be0b3fa3-ed37-4ef4-bee9-e3029dd82962"]},Open],ExpressionUUID -> "9e5d74a8-80a8-4b54-b46f-2e525ee0a159"],Cell[
CellGroupData[
{Cell[
TextData[{"2a",")"}],"Subsection",ExpressionUUID -> "afc92126-243f-4de6-82d0-e1b2f9cd82a8"],Cell[
BoxData[
RowBox[{RowBox[{"n"," ","="," ","100"}],";"}]],"Input",CellLabel -> "In[66]:= ",ExpressionUUID -> "c43e0c89-f7b7-47a6-a74d-156c9072c947"],Cell[
BoxData[
RowBox[{RowBox[{"a0"," ","="," ",RowBox[{"5.29","*",RowBox[{"10","^",RowBox[{"{",RowBox[
{"-","9"}],"}"}]}]}]}],";"}]],"Input",CellLabel -> "In[67]:= ",ExpressionUUID -> "ac1aad79-180f-4741-a019-7a9571e6b159"],Cell[
CellGroupData[
{Cell[
BoxData[RowBox[{"qn"," ","="," ",RowBox[{RowBox[{"(",RowBox[{"1","/",RowBox[
{"(",RowBox[{RowBox[{"n","^",RowBox[{"(",RowBox[{"1","/","3"}],")"}]}],"*","a0"}],")"}]}],")"}],"^",RowBox[
{"(",RowBox[{"1","/","2"}],")"}]}]}]],"Input",CellLabel -> "In[68]:= ",ExpressionUUID -> "952a5696-563c-4b73-b7e8-c2cd96914035"],Cell[
BoxData[
RowBox[{"{","6381.735946182911`","}"}],StandardForm],"Output",CellLabel -> "Out[68]= ",
ExpressionUUID -> "a7639713-8290-449f-93a2-7d9ea101e575"]},Open],ExpressionUUID -> "078be561-17bc-405d-a5ad-c619e37c4f82"]},
Open],ExpressionUUID -> "6d81b043-a557-41ff-b763-5851f309ced6"],Cell[
CellGroupData[
{Cell[
TextData[{"2b",")"}],"Subsection",ExpressionUUID -> "1b81e08a-9108-4624-a385-c4b8ede16563"],Cell[
TextData[
{"Let"," ","the"," ","cross"," ","section"," ","we"," ","are"," ","looking"," ","into"," ","be"," ","in"," ","cm","^","2"}],
"Text",ExpressionUUID -> "b298becb-dd1e-4c27-b408-feabdcc12327"],Cell[
BoxData[RowBox[
{"\[Sigma]e"," ","=",RowBox[{"1","*",RowBox[{"10","^",RowBox[{"{",";"}]}]}]}]],"Input",
ExpressionUUID -> "8b4022c9-71dd-4764-80b6-9ab8810ac1be"],Cell[
BoxData[RowBox[{RowBox[
{"evtoergs"," ","="," ",RowBox[{"1.602","*",RowBox[{"10","^",RowBox[{"{",RowBox[{"-","12"}],"}"}]}]}]}],";"}]],
"Input",CellLabel -> "In[74]:= ",ExpressionUUID -> "3bb73caf-fcd6-4c4b-80d7-e49e9a7fb0f9"],Cell[
BoxData[
RowBox[{"Clear","[","n","]"}]],"Input",CellLabel -> "In[70]:= ",ExpressionUUID -> "5e4dcb13-d92f-4dbc-b4b7-24c60ae0f894"],Cell[
CellGroupData[
{Cell[
BoxData[RowBox[{"Solve","[",RowBox[{RowBox[{"\[Sigma]e"," ","=="," ",RowBox[
{RowBox[{"Pi","*",RowBox[{RowBox[{"(",RowBox[{"a0","/",RowBox[{"(",RowBox[{"16","*","n"}],")"}]}],")"}],"^","2"}]}]," ","+"," ",RowBox[
{RowBox[{"(",RowBox[{"Pi","*",RowBox[{RowBox[{"echarge","^","2"}],"/",RowBox[{"(",RowBox[
{"27.2","*","evtoergs"}],")"}]}]}],")"}],"*",RowBox[{"(",RowBox[{"a0","/",RowBox[
{"(",RowBox[{"16","*","n"}],")"}]}],")"}],"*",RowBox[{RowBox[{"(",RowBox[{RowBox[
{"(",RowBox[{RowBox[{"2","*","n"}],"+","1"}],")"}],"/",RowBox[{"(",RowBox[{RowBox[
{"n","^","2"}],"*",RowBox[{RowBox[{"(",RowBox[{"n","+","1"}],")"}],"^","2"}]}],")"}]}],")"}],"^",RowBox[
{"{",RowBox[{"-","1"}],"}"}]}]}]}]}],",","n"}],"]"}]],"Input",CellLabel -> "In[86]:= ",
ExpressionUUID -> "21cf6d7d-8e28-4b54-9ecf-12802697be11"],Cell[
BoxData[TemplateBox[
{"Solve","ratnz","\"Solve was unable to solve the system with inexact coefficients. The answer was obtained by solving a corresponding exact system and numericizing the result.\"",2,86,13,19311141330849475938,"Local"},
"MessageTemplate"],StandardForm],"Message","MSG",ExpressionUUID -> "54b61dc5-ec55-42ec-8d3f-68e4d9ec22a0"],Cell[
BoxData[
RowBox[{"{",RowBox[{RowBox[{"{",RowBox[{"n","\[Rule]",RowBox[{"-","60306.24241389241`"}]}],"}"}],",",RowBox[
{"{",RowBox[{"n","\[Rule]",RowBox[{"-","0.5000000000343713`"}]}],"}"}],",",RowBox[
{"{",RowBox[{"n","\[Rule]",RowBox[{"-","5.860175544556354`*^-6"}]}],"}"}],",",RowBox[
{"{",RowBox[{"n","\[Rule]","5.860175544556372`*^-6"}],"}"}],",",RowBox[{"{",RowBox[
{"n","\[Rule]","60304.74241389245`"}],"}"}]}],"}"}],StandardForm],"Output",CellLabel -> "Out[86]= ",
ExpressionUUID -> "e37eef94-90b6-45af-a883-e612b108aa1f"]},Open],ExpressionUUID -> "1a7e9354-2950-4087-bb44-08be5148c9ba"]},
Open],ExpressionUUID -> "409097de-d6a5-403a-8f5b-a6d78bfd73e0"]},StyleDefinitions -> "Default.nb",
FrontEndVersion -> "12.2 for Wolfram Cloud 1.57.0.2 (December 8, 2020)"]