program fortrantut
  implicit none
  real :: float_num = 1.111111111111111
  real :: float_num2 = 1.111111111111111
  double precision :: dbl_num = 1.1111111111111111d+0
  double precision :: dbl_num2 = 1.1111111111111111d+0
  real :: rand(1)
  integer :: low = 1, high = 10
  integer :: n=0, m=1
  integer :: secret_num = 7
  character (len=30) :: str = "I'm a string"
  character (len=30) :: str2 = " that is longer"
  character (len=30) :: str3

  print "(a8, i1)", "5 + 4 = ", (5+4)
  print "(a8, i1)", "5 - 4 = ", (5-4)
  print "(a8, i2)", "5 * 4 = ", (5*4)
  print "(a8, i1)", "5 / 4 = ", mod(5,4)
  print "(a8, i3)", "5 ** 4 = ", (5**4)

  !precision
  print "(f17.15)", float_num + float_num2
  print "(f17.15)", dbl_num + dbl_num2

  call random_number(rand)
  print "(i2)", low + floor((high +1 - low)*rand)

  ! do loops

  do while (m<20)
     if(mod(m,2) == 0) then
        print "(i2)", m
        m = m+1
        cycle
     end if
     m = m+1
     if (m>=12) then
        exit
     end if
     end do

     str3 = trim(str) // trim(str2)
     print *, str3
  
  

end program fortrantut
