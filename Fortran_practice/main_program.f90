program main_program
  use mult_mod
  implicit none
  real :: r_ans
  integer :: i=1, p1, p2

  print "(a8,i2)", "5*4 = ", mult(5,4)
  r_ans = mult(5.3,4.4)

  print "(a12,f6.2)", "5.3*4.4 = ", r_ans

  call plus_two(i,p1, p2)
  print "(i1,/,i1,/,i1)", i, p1, p2 ! you can declare the name of the inputs so you can read them externally from the subroutine

contains
  subroutine plus_two(n,plus1,plus2)
    integer, intent(in) :: n
    integer, intent(out) :: plus1, plus2
    plus1 = n +1
    plus2 = n+2
  end subroutine plus_two
  
  
    

end program main_program
