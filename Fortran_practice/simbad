#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Sun Mar  8 23:51:23 2020

@author: korashassani
"""

import os
#---importpylabstuff
import pylab as plt
import numpy as np
#---importastropystuff
from astropy.table import Table,Column
#---importastroquerystuff
from astroquery.vizier import Vizier
from astroquery.simbad import Simbad

v = Vizier(columns=['all'])
catalist = Vizier.find_catalogs('J/A+A/420/183')
Vizier.ROW_LIMIT = -1
s4n = Vizier.get_catalogs(catalist.keys())

#---retainonlytargetsforwhichall3parametershavebeenset
filter=(s4n[0]['Teff'].mask==False)&(s4n[0]['logg'].mask==False)&\
(s4n[0]['__Fe_H_'].mask==False)
#---computenumberoftargets
ntargets=len(s4n[0][filter])
#---weshallneedtorestorefullHIP#forSimbadqueries...
objhip=[]
#---applyfiltertotables
obj=s4n[0]['HIP'][filter]
teff=s4n[0]['Teff'][filter]
logg=s4n[0]['logg'][filter]
mtal=s4n[0]['__Fe_H_'][filter]

#---nowconverttousual(dex)metallicity
mtal=mtal-7.55

#---restoringfullHIPnamesfornext(simbad)queries
for i in np.arange(ntargets):
    objhip.append('HIP'+str(obj[i]))
#---nowretrievingvsinivalue(mostrecentthough)fromSimbad
Simbad.add_votable_fields('rot')
vsini=np.zeros((ntargets),'Float64')
for i in np.arange(1,ntargets):
    t=Simbad.query_object(objhip[i])
    if (len(t)<0):
        vsini[i]=t['ROT_Vsini'][0]
    else:
        vsini[i]=-1.



#---graphicalsummary/output
plt.figure(1)
plt.scatter(teff,logg,s=50*(mtal+1),c=mtal)
plt.title('S4N(AllendePrietoetal.2004)',fontsize=20)
plt.xlabel('effectivetemperature[K]',fontsize=20)
plt.ylabel('surfacegravity[dex]',fontsize=20)
cb=plt.colorbar()
cb.set_label('[Fe/H]metallicity',size=20)
plt.figure(2)
plt.scatter(teff,logg,s=vsini,c=vsini)
plt.title('S4N(AllendePrietoetal.2004)',fontsize=20)
plt.xlabel('effectivetemperature[K]',fontsize=20)
plt.ylabel('surfacegravity[dex]',fontsize=20)
cb=plt.colorbar()
cb.set_label('vsini(Simbad)',size=20)
plt.show()