#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Fri May 22 11:12:30 2020

@author: korashassani
"""

# Regression
import pandas as pd
import quandl
import math
import datetime
import numpy as np
#cross validation is a nice time savor, svm is suport vector machine
from sklearn import preprocessing, model_selection, svm
from sklearn.linear_model import LinearRegression
from sklearn.model_selection import train_test_split
from sklearn import datasets
from sklearn import svm
import matplotlib.pyplot as plt
from matplotlib import style
import pickle
style.use('ggplot')



# import warnings filter
from warnings import simplefilter
# ignore all future warnings
simplefilter(action='ignore', category=FutureWarning)

quandl.ApiConfig.api_key = "q-r-PPnQE-K4zzyTngzQ"


df = quandl.get('WIKI/GOOGL')

df = df[['Adj. Open','Adj. High','Adj. Close','Adj. Volume']]

df['HL_PCT'] = (df['Adj. High']-df['Adj. Close'])/df['Adj. Close'] * 100.0 
df['PCT_change'] = (df['Adj. Close']-df['Adj. Open'])/df['Adj. Open'] * 100.0 

df = df[['Adj. Close','HL_PCT','PCT_change','Adj. Volume']]



# Features and Labels
# Generally HL_PCT and PCT_change are are main features to work with.
# Predicting say the adj. close for the future

forecast_col = 'Adj. Close'
#na is nan
df.fillna(-99999, inplace = True)

#regression algorithm forcast out 

forecast_out = int(math.ceil(.01*len(df))) # trying to forecast 10 days out 
print('We are predicting ', forecast_out,'days out') # how many days in advance

df['label'] = df[forecast_col].shift(-forecast_out) # shifting column up or negatively



#define x and y

x = np.array(df.drop(['label'],1)) # everything but label column 1 refers to linear
x = preprocessing.scale(x)
x = x[:-forecast_out]
x_lately = x[-forecast_out:] # stuff we're predicting against
 
df.dropna(inplace = True)
y = np.array(df['label'])


x_train, x_test, y_train, y_test = model_selection.train_test_split(x,y,test_size =.2) # takes x and ys and shuffles them up and outputs the training and test data used to fit classifier

#define classifier to predict for the future

clf = LinearRegression(n_jobs = -1) #n_jobs allows you to run in parallel using n threads (-1 refers to all that you could use)
clf.fit(x_train,y_train) # synonymous with train
accuracy1 = clf.score(x_test,y_test) # synonymous with test

print('Linear Regression acuraccy =', accuracy1)

#different algorithm support vector (not as good here)

clf = svm.SVR(kernel = 'poly')
clf.fit(x_train,y_train) # synonymous with train
accuracy2 = clf.score(x_test,y_test) # synonymous with test

print('svm SVR polynomial acuraccy =', accuracy2)

clf = svm.SVR(kernel = 'linear')
clf.fit(x_train,y_train) # synonymous with train
accuracy3 = clf.score(x_test,y_test) # synonymous with test

print('svm SVR linear acuraccy =', accuracy3)


#forecasting and predicting
forecast_set = clf.predict(x_lately)

print('For the next', forecast_out, 'days', 'we expect the following forecast:', forecast_set,'with accuracy =', accuracy1)

df['Forecast'] = np.nan

##find last date
last_date = df.iloc[-1].name
last_unix = last_date.timestamp()
one_day = 86400
next_unix = last_unix+one_day

## populate data frame with new dates and forecast value
for i in forecast_set:
    next_date = datetime.datetime.fromtimestamp(next_unix)
    next_unix += one_day
    df.loc[next_date] = [np.nan for _ in range(len(df.columns)-1)] + [i] #.loc references index for dataframe. It's basically a date stamp
    

df['Adj. Close'].plot()
df['Forecast'].plot()
plt.legend(loc=4)
plt.xlabel('Date')
plt.ylabel('Price')

plt.show()


# Pickling and Scaling
## pickling is serialization of any python project


