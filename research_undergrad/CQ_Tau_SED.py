#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Sat Feb 29 21:13:08 2020

@author: korashassani
"""
import os
#create output directory

#good fit for bright phase - WH = 1
#output = '/Users/korashassani/Documents/CQTauri/Saved Outputs/current outputs/brightnew/output_bright_3_5_2020_best_thus_far_bass_not_great/'
output = '/Users/korashassani/Documents/research/Research - Sitko/CQ Tau/Output_correct_inc/'

#obscured phases 
#output = '/Users/korashassani/Documents/CQTauri/Saved Outputs/current outputs/brightnew/output_obscured_off_longrun_edits/'


#change working directory to sed folder

os.chdir('/Users/korashassani/Documents/research/Research - Sitko/CQ Tau/SED/')


#read in save files

import scipy.io as sio
import matplotlib.pyplot as plt
import numpy as np
import matplotlib.mlab as mlab
import pylab as plti
from astropy.io import fits
from astropy.io.fits import info
from astropy.utils.data import get_pkg_data_filename
from astropy.io.ascii import read
import pandas as pd

#set plot type & range. log(x),y
plt.figure()
axes = plt.gca()
axes.set_xlim([1e-1,5e3])
axes.set_ylim([1e-17,1e-10])
plt.xscale('log')
plt.yscale('log')
plt.title('CQ Tau SED Model')
plt.xlabel('\u03BB (\u03BCm)')
plt.ylabel('\u03BBF(\u03BB) (W/m^2)')






#photometry
#ubv = sio.readsav('/Users/korashassani/Documents/CQTauri/Saved Outputs/current outputs/brightnew/SED/cqtau_ubvrijhkX.sav', idict=None, python_dict=False, uncompressed_file_name=None, verbose=False)
ubv = sio.readsav('cqtau_ubvrijhkX.sav', idict=None, python_dict=False, uncompressed_file_name=None, verbose=False)
lam = ubv('w')
lfl1 = ubv('lfl1') # 10/25/1998
lfl2 = ubv('lfl2') # 10/26/1998
lfl3 = ubv('lfl3') # 10/27/1998
elfl3 = [8.959e-14,1.73e-13,1.51e-13, 2.06e-13,1.54e-13,0,0,0]
lfl5 = ubv('lfl5') # 01/31/1999
elfl5 = [5.93e-15,4.96e-14,4.45e-14,3.86e-14,2.98e-13,0,0,0]
#plt.scatter(lam,lfl1, label = 'Photometry 10/25/1998')
#plt.scatter(lam,lfl2, label = 'Photometry 10/26/1998')
plt.scatter(lam,lfl3, label = 'Photometry 10/27/1998')
plt.scatter(lam,lfl5 , label = 'Photometry 01/31/1999')


#bass data 10/14/1996
bass1 = sio.readsav('f_cqtaum_bass-961014_rev.sav', idict=None, python_dict=False, uncompressed_file_name=None, verbose=False)
lam1 = bass1('w')
lflb1 = bass1('lfl')
elfl1 = bass1('elfl')
plt.scatter(lam1,lflb1 , label = 'Bass 10/14/1996')


#bass data 08/17/07
bass2 = sio.readsav('f_cqtaum_bass_070817.sav', idict=None, python_dict=False, uncompressed_file_name=None, verbose=False)
lam2 = bass2('w')
lflb2 = bass2('lfl')
elflb2 = bass2('elfl')
plt.scatter(lam2,lflb2, label = 'Bass 08/17/2007')

#bass data 10/24/10
bass3 = sio.readsav('f_cqtaum_bass_101024_rev.sav', idict=None, python_dict=False, uncompressed_file_name=None, verbose=False)
lam3 = bass3('w')
lflb3 = bass3('lfl')
elflb3 = bass3('elfl')
plt.scatter(lam3,lflb3, label = 'Bass 10/24/2010')

#SpeX
#Spex = fits.getdata('/Users/korashassani/Documents/CQTauri/Saved Outputs/current outputs/brightnew/SED/cqtau_xmergexd_101125.fits')
#lams = Spex[0,:-320]
#lfls= Spex[1,:-320]
Spex = sio.readsav( 'cqtau_xmergexd_101125x0.85.sav', idict=None, python_dict=False, uncompressed_file_name=None, verbose=False)
lams = Spex['w']
lfls = Spex['lfl']

plt.plot(lams,lfls,label = 'Spex 11/12/2010')

#prism
Spex = sio.readsav( 'cqtau_prism.sav', idict=None, python_dict=False, uncompressed_file_name=None, verbose=False)
lamp = Spex['w']
lflp = Spex['lfl']

plt.plot(lamp,lflp,label = 'Prism 11/25/2010')

#mm data

mmTNSW = sio.readsav('cqtau_mmTNSW.sav', idict=None, python_dict=False, uncompressed_file_name=None, verbose=False)
lammt = mmTNSW['w']
lflt = mmTNSW['lfl']
elflt = mmTNSW['elfl']
plt.errorbar(lammt,lflt, yerr = elflt, label = 'mmTNSW 7 mm', fmt = 'o')


mmMS97 = sio.readsav('cqtau_mmMS97.sav', idict=None, python_dict=False, uncompressed_file_name=None, verbose=False)

lammms = mmMS97['w'][0]
lflms = mmMS97['lfl'][0]
elflms = mmMS97['elfl'][0]
lammms2 = mmMS97['w'][1]
lflms2 = mmMS97['lfl'][1]
elflms2 = mmMS97['elfl'][1]
plt.errorbar(lammms,lflms, yerr = elflms, label = 'mmMS97 1.3 mm', fmt = 'o')
plt.errorbar(lammms2,lflms2,yerr = elflms2, label = 'mmMS97 2.7 mm', fmt = 'o')

#IRAS
iras = sio.readsav('cqtau_iras.sav', idict=None, python_dict=False, uncompressed_file_name=None, verbose=False)
lami = iras['w']
lfli = iras['lfl']
elfli = iras['elfl']
plt.errorbar(lami,lfli, yerr = elfli, label = 'IRAS', fmt = 'o')

#HERSEL
herschel = sio.readsav('cqtau_herschelPACS.sav', idict=None, python_dict=False, uncompressed_file_name=None, verbose=False)

lamh = herschel['w']
lflh = herschel['lfl']
elflh = herschel['elfl']
plt.errorbar(lamh,lflh, yerr = elflh, label = 'Hershel', fmt = 'o')

#2mass
bass1 = sio.readsav('cqtau_2mass.sav', idict=None, python_dict=False, uncompressed_file_name=None, verbose=False)
lam1 = bass1('w')
lflb1 = bass1('lfl')
elfl1 = bass1('elfl')
plt.scatter(lam1,lflb1 , label = '2MASS')

#IUE
bass1 = sio.readsav('iue_lwp09310.sav', idict=None, python_dict=False, uncompressed_file_name=None, verbose=False)
#print(bass1)
lam1 = bass1('w')
lflb1 = bass1('lflsmooth11')
#elfl1 = bass1('elfl')
plt.scatter(lam1,lflb1 , label = 'IUE 08/09/2010')

bass1 = sio.readsav('iue_lwp31511.sav', idict=None, python_dict=False, uncompressed_file_name=None, verbose=False)
#print(bass1)
lam1 = bass1('w')
lflb1 = bass1('lflsmooth11')
#elfl1 = bass1('elfl')
plt.scatter(lam1,lflb1 , label = 'IUE 05/31/2011')


# MSX
#bass1 = sio.readsav('MSX_corrected.sav', idict=None, python_dict=False, uncompressed_file_name=None, verbose=False)
#print(bass1)
#lam1 = bass1('w')
#lflb1 = bass1('lfl')
#elfl1 = bass1('elfl')
#plt.scatter(lam1,lflb1 , label = 'MSX')

#AKARI_FIS
bass1 = sio.readsav('AKARI_FIS.sav', idict=None, python_dict=False, uncompressed_file_name=None, verbose=False)
#print(bass1)
lam1 = bass1('w')
lflb1 = bass1('lfl')
#elfl1 = bass1('elfl')
plt.scatter(lam1,lflb1 , label = 'AKARI FIS')

bass1 = sio.readsav('AKARI_IRC.sav', idict=None, python_dict=False, uncompressed_file_name=None, verbose=False)
#print(bass1)
lam1 = bass1('w')
lflb1 = bass1('lfl')
#elfl1 = bass1('elfl')
plt.scatter(lam1,lflb1 , label = 'AKARI IRS')

#WISE
bass1 = sio.readsav('WISE.sav', idict=None, python_dict=False, uncompressed_file_name=None, verbose=False)
#print(bass1)
lam1 = bass1('w')
lflb1 = bass1('lfl')
#elfl1 = bass1('elfl')
plt.scatter(lam1,lflb1 , label = 'WISE')



# read in model

Av = 0.4491  # assumes foreground dust is similar to ISM.
atfile = '/Users/korashassani/Downloads/kt06500g+4.0z+0.0.ascii'
#atfile = '/Users/korashassani/Documents/CQTauri/Saved Outputs/current outputs/parfiles/kt06750g+2.0z+0.5.ascii'
d = 163.1188 #  distance in pc
tstar = 6720
rstar = 2.077


# What to plot

iplotstar = 1  # 1 for plotting stellar spectrum, 0 for not
flux = 'a' # this determines what particular flux you plot. 'a' plots total flux


#nap = 1-1l  #aperture stuff, not sure what else it does
lumscale = 1.0e0 #luminosity scale factor, default=1, but you might be trying to fit some data so this lets you see how much you need to change it (run lum.pro to see what rstar and tstar you need).


#calculate fnorm

c = 2.9979e14
sigma = 5.67e-5
pc = 3.0857e18
lsun = 3.845e33
lumstar = 4.0e0*np.pi*(rstar*6.955e10)**2*sigma*tstar**2*tstar**2
rstar2 = lumstar/(4.0e0*np.pi*sigma)/tstar**2/tstar**2
fnorm = lumscale/(4.0e0*np.pi)/d**2/pc/pc*lsun










# Stellar atmosphere

atmnorm = rstar2/d/d/pc/pc
atmfile = read(atfile) # returns a table of wavelength vs. intenstity for atmospheric file
atmarray = np.array(atmfile) # Copy table data to numpy structured array object
lama = np.array(atmfile['col1'])
hnu = np.array(atmfile['col2'])
nuhnu = hnu/lama*2.9979e14
nuhnu = nuhnu*atmnorm

# read in model
# info for peel file
peelfile = output+'peel_hypercube.fits.gz'

fluxarr = fits.getdata(peelfile, ext = 0)
wavearr = fits.getdata(peelfile, ext = 1)
peelarr = fits.getdata(peelfile, ext = 2)
aparr = fits.getdata(peelfile, ext = 3)

nfreq= fits.getval(peelfile,'NAXIS1')
npeel = fits.getval(peelfile,'NAXIS2')
napmax = fits.getval(peelfile, 'NAXIS3')
no = fits.getval(peelfile, 'NAXIS4')


x5 = [item[0] for item in wavearr]
peeltheta = [item[0] for item in peelarr]
peelphi = [item[1] for item in peelarr]
aps = [item[0] for item in aparr]

f5 = fluxarr[0,0,:,:,:]
f5d = fluxarr[0,5,:,:,:]
f5s = fluxarr[0,6,:,:,:]
f5t = fluxarr[0,7,:,:,:]

f5 = f5*fnorm
f5d = f5d*fnorm
f5s = f5s*fnorm
f5t = f5t*fnorm

nuhnu = nuhnu/1000
f5 = f5/1000
f5 = np.squeeze(f5)

plt.plot(x5,f5 , label = 'Model')
plt.plot(lama,nuhnu, label = 'Atmosphere' )

#IRAS
plt.legend(loc = 'upper center',bbox_to_anchor = (1.3,1.0),shadow = True, ncol = 1)


import matplotlib.transforms
bbox = matplotlib.transforms.Bbox([[-0.2, -.5], [8, 5]])
plt.savefig('CQ_Tau_SED_python', dpi = 150,bbox_inches = bbox)












