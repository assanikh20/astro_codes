#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Tue Feb 25 22:21:40 2020

@author: korashassani
"""

#julian date respective to magnitudes
jul = [2451111.702,2451112.644,2451113.729,2451114.642]#,2451209.562]
jul2 = [2451111.686,2451112.65,2451113.655]#,2451208.48,2451208.519,2451209.497,2451209.527,2451210.537,2451210.583


#uband magnitude Oudmaijer et al.
uwav = 0.365 #micrometers
umag = [10.54,10.44,9.75,9.92]#,10.93]
uerr = [0.03,0.05,0.05,0.02]#,0.01]



# band magnitude from Oudmaijer et al.
bwav = 0.445 #micrometers
bmag = [10.1,10.1,9.5,9.65]#,10.5]
berr = [0.03,0.1,0.04,0.03]#,0.03]



#v band magnitude from Oudmaijer et al.
vwav = 0.551 #micrometers
vmag = [9.45,9.44,8.98,9.1]#,9.86]
verr = [0.03,0.07,0.03,0.03]#,0.02]

#r band magnitude '''''
rwav = 0.658
rmag = [9.08,9.02,8.63,8.75]#,9.44]
rerr = [0.04,0.09,0.05,0.03]#,0.02]

# I band magnitude '''''
iwav = 0.806
imag = [8.73,8.64,8.33,8.45]#,9.02]
ierr = [0.03,0.08,0.04,0.03]#,0.1]


#j band magnitude ''''
jwav = 1.22
jmag = [7.89,8.04,7.76]#,8.27,8.29,8.08,8.08,7.89,7.83]
#jerr = 

#h band magnitude ''''
hwav = 1.63
hmag = [7.14,7.28,7.09]#,7.27,7.29,7.13,7.15,7.12,7.08]

#k band magnitude ''''
kwav = 2.19
kmag = [6.3,6.42,6.31]#,6.29,6.23,6.3,6.29]

import matplotlib.pyplot as plt
import numpy as np
import matplotlib.mlab as mlab
import pylab as plti


plt.plot(jul, umag,color='c', label = 'u')
plt.plot(jul, bmag,color='b', label = 'b')
plt.plot(jul, vmag,color='y', label = 'v')
plt.plot(jul, rmag,color='r', label = 'r')
plt.plot(jul, imag,color='g', label = 'i')
plt.plot(jul2, jmag,color='m', label = 'j')
plt.plot(jul2, hmag,color='k', label = 'h')
plt.plot(jul2, kmag,color='c', label = 'k')
plt.gca().invert_yaxis()


plt.legend(('u', ' b', 'v', 'r', 'i','j','h','k'))




plt.errorbar(jul, umag, yerr=uerr, fmt='.',ecolor='r',color='c')
plt.errorbar(jul, bmag, yerr=berr, fmt='.',ecolor='r',color='b')
plt.errorbar(jul, vmag, yerr=verr, fmt='.',ecolor='r',color='y')
plt.errorbar(jul, rmag, yerr=rerr, fmt='.',ecolor='r',color='g')
plt.errorbar(jul, imag, yerr=ierr, fmt='.',ecolor='r',color='r')


plt.title("Light Curves of CQ Tau")
plt.xlabel("Julian Date")
plt.ylabel("Magnitude")
plt.savefig("lightcurvecqtau",dpi=150)

plt.show()

# this is from Kozlova et al. 2000 & 2007
julong = [95030,95058,95061,95062,95064,95255,95266,95287,95299,96018,96024,96049,96056,96070,96080,97281,97282,97305,97306,97308,97362,98003,98049,98051,98057,98059,98060,98303,98323,98347,98349,99018,99020,99048,99274,99275,99276,99343,99345,99346,99347,2321]
julongv = [95030,95058,95061,95062,95064,95255,95266,95287,95299,96018,96024,96070,96080,97281,97282,97305,97306,97308,97362,98003,98049,98057,98059,98060,98303,98323,98347,98349,99018,99020,99048,99274,99275,99276,99343,99345,99346,99347]

vmaglong = [10.4,9.62,10.59,10.18,9.91,11.28,10.4,11.1,10.8,11.51,11.36,9.57,10.31,10.68,9.69,9.77,9.64,10.11,9.31,9.45,9.44,9.56,9.72,9.91,9.42,9.61,9.78,9.64,9.47,9.43,9.86,9.66,9.85,9.63,9.08,9.15,9.14,9.2]

plt.plot(julongv,vmaglong, color = 'y',linestyle = 'dotted')
plt.gca().invert_yaxis()
plt.title("Longterm Light Curve of CQ Tau")
plt.xlabel("Julian Date")
plt.ylabel("Magnitude")






